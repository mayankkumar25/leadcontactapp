//This is an example code for Bottom Navigation//
import React from 'react';
import {Button, Text, View, TouchableOpacity, StyleSheet} from 'react-native';
//import all the basic component we have used
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//import Ionicons to show the icon for bottom options

//import React Navigation
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import LeadScreen from './src/screens/LeadScreen';
import ContactScreen from './src/screens/ContactScreen';
import ListScreen from './src/screens/ListScreen';
import LoginScreen from './src/screens/LoginScreen';

const LeadStack = createStackNavigator(
  {
    //Defination of Navigaton from Leads screen
    Lead: {screen: LeadScreen},
    List: {screen: ListScreen},
  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: 'orange',
      },
      headerTintColor: '#FFFFFF',
      title: 'Leads',
      //Header title
    },
  },
);

const LoginStack = createStackNavigator(
  {
    Login: {screen: LoginScreen},
  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: 'orange',
      },
      headerTintColor: '#FFFFFF',
      title: 'Login',
      //Header title
    },
  },
);

const ContactStack = createStackNavigator(
  {
    //Defination of Navigaton from Contact screen
    Contact: {screen: ContactScreen},
    List: {screen: ListScreen},
  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: 'orange',
      },
      headerTintColor: '#FFFFFF',
      title: 'Contacts',
      //Header title
    },
  },
);

const App = createBottomTabNavigator(
  {
    Login: {screen: LoginStack},
    Lead: {screen: LeadStack},
    Contact: {screen: ContactStack},
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let IconComponent = MaterialIcons;
        let iconName;
        if (routeName === 'Lead') {
          iconName = `person${focused ? '' : '-outline'}`;
        } else if (routeName === 'Contact') {
          iconName = `people${focused ? '' : '-outline'}`;
        } else if (routeName === 'Login') {
          iconName = `vpn-key`;
        }
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'orange',
      inactiveTintColor: 'gray',
    },
  },
);
export default createAppContainer(App);
