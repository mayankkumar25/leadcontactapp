import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';
import FormComponent from '../components/FormComponent';

export default class LeadScreen extends Component {
  render() {
    return (
      <View>
        <FormComponent page="leads" />
        <Button
          style={styles.button}
          color="orange"
          mode="outlined"
          onPress={() =>
            this.props.navigation.navigate('List', {page: 'leads'})
          }>
          SHOW LEADS
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 50,
    marginBottom: 100,
    width: 150,
    alignSelf: 'center',
  },
});
