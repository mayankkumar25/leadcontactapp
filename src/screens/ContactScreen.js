import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';
import FormComponent from '../components/FormComponent';

export default class ContactScreen extends Component {
  render() {
    return (
      <View>
        <FormComponent page="contacts" />
        <Button
          style={styles.button}
          color="orange"
          mode="outlined"
          onPress={() =>
            this.props.navigation.navigate('List', {page: 'contacts'})
          }>
          SHOW CONTACTS
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 50,
    marginBottom: 100,
    width: 200,
    alignSelf: 'center',
  },
});
