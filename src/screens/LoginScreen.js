import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Button, Snackbar} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import {BASEURL, INPUT_TYPE, RESPONSE_TYPE} from './../config/base_url';
import axios from 'axios';

export default class LoginScreen extends Component {
  state = {
    visible: false,
    snackMsg: '',
    login: true,
    method_name: 'login',
    session: '',
  };
  handleLogin = () => {
    const RESTDATA = this.state.login
      ? {
          user_auth: {
            user_name: 'admin',
            password: '21232f297a57a5a743894a0e4a801fc3',
            version: '4.1',
          },
          application_name: 'foo',
        }
      : {session: this.state.session};

    //Login API Request
    axios
      .get(BASEURL, {
        params: {
          method: this.state.method_name,
          input_type: INPUT_TYPE,
          response_type: RESPONSE_TYPE,
          rest_data: RESTDATA,
        },
      })
      .then(async (resp) => {
        if (resp.data !== null) {
          const sessionID = resp.data.id;
          if (sessionID !== null) {
            //Set the Session in LocalStorage
            await AsyncStorage.setItem('SESSION_ID', sessionID);
            //Set Snackbar
            this.setState({
              visible: true,
              login: false,
              method_name: 'logout',
              session: sessionID,
              snackMsg: 'Session ID Stored Successfully!',
            });
          }
        } else {
          await AsyncStorage.removeItem('SESSION_ID');
          //Set Snackbar
          this.setState({
            visible: true,
            login: true,
            method_name: 'login',
            session: '',
            snackMsg: 'Logout Successful!',
          });
        }
      })
      .catch((err) => console.log('ERROR: ', err));
  };

  _onDismissSnackBar = () => this.setState({visible: false});

  render() {
    return (
      <View>
        <Button
          mode="outlined"
          color={this.state.login ? 'orange' : 'red'}
          style={{
            marginTop: 300,
            width: 100,
            alignSelf: 'center',
          }}
          onPress={this.handleLogin}>
          {this.state.login ? 'LOGIN' : 'LOGOUT'}
        </Button>
        <Snackbar
          style={{marginTop: 100}}
          duration={3000}
          visible={this.state.visible}
          onDismiss={this._onDismissSnackBar}>
          {this.state.snackMsg}
        </Snackbar>
      </View>
    );
  }
}
