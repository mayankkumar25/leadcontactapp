import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {BASEURL, INPUT_TYPE, RESPONSE_TYPE} from './../config/base_url';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

import AntIcon from 'react-native-vector-icons/AntDesign';

export default class ListScreen extends Component {
  state = {
    module_name:
      this.props.navigation.getParam('page') === 'leads' ? 'Leads' : 'Contacts',
    listData: '',
    visible: false,
  };

  componentDidMount = async () => {
    //Get session value from localstorage
    const session_value = await AsyncStorage.getItem('SESSION_ID');
    if (session_value !== null) {
      //Get Leads form the server
      axios
        .get(BASEURL, {
          params: {
            method: 'get_entry_list',
            input_type: INPUT_TYPE,
            response_type: RESPONSE_TYPE,
            rest_data: {
              session: session_value,
              module_name: this.state.module_name,
            },
          },
        })
        .then((resp) => {
          if (resp.data.name === 'Invalid Session ID') {
            this.setState({visible: true});
          } else {
            this.setState({listData: resp.data.entry_list});
          }
        })
        .catch((err) => console.log('ERROR: ', err));
    } else {
      this.setState({visible: true});
    }
  };

  deleteRecordWithId = async (idValue) => {
    const session_value = await AsyncStorage.getItem('SESSION_ID');
    axios
      .get(BASEURL, {
        params: {
          method: 'set_entry',
          input_type: INPUT_TYPE,
          response_type: RESPONSE_TYPE,
          rest_data: {
            session: session_value,
            module_name: this.state.module_name,
            name_value_list: {
              id: {name: 'id', value: idValue},
              deleted: {name: 'deleted', value: '1'},
            },
          },
        },
      })
      .then((resp) => {
        const newList = this.state.listData.filter(
          (item) => item.name_value_list.id.value !== idValue,
        );
        this.setState({listData: newList});
      })
      .catch((err) => console.log('ERROR: ', err));
  };

  _onDismissSnackBar = () => this.setState({visible: false});

  render() {
    return (
      <SafeAreaView>
        {this.state.visible ? (
          <Text
            style={{
              alignSelf: 'center',
              margin: 15,
              fontSize: 30,
              fontWeight: 'bold',
            }}>
            Oops! Invalid Session ID. Please Login Again
          </Text>
        ) : (
          <FlatList
            data={this.state.listData}
            renderItem={({item}) => (
              <View style={styles.listItem}>
                <Text style={styles.itemName}>
                  {item.name_value_list.full_name.value}
                </Text>
                <View style={styles.itemDelete}>
                  <TouchableOpacity
                    onPress={() =>
                      this.deleteRecordWithId(item.name_value_list.id.value)
                    }>
                    <AntIcon name="deleteuser" color="red" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            )}
            keyExtractor={(item) => item.id}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    padding: 10,
    margin: 5,
    flex: 1,
    flexDirection: 'row',
  },
  itemName: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  itemDelete: {
    flex: 1,
    alignItems: 'flex-end',
  },
});
