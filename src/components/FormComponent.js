import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {TextInput, Button, Snackbar} from 'react-native-paper';

import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {INPUT_TYPE, RESPONSE_TYPE, BASEURL} from '../config/base_url';

export default class FormComponent extends Component {
  state = {
    firstName: '',
    lastName: '',
    website: '',
    status: '',
    MODULE_NAME:
      this.props.page === 'leads'
        ? 'Leads'
        : this.props.page === 'contacts'
        ? 'Contacts'
        : '',
    visible: false,
    snackMsg: '',
  };

  _onDismissSnackBar = () => this.setState({visible: false});

  createNewRecord = async () => {
    const session_value = await AsyncStorage.getItem('SESSION_ID');

    if (session_value !== null) {
      //Call API to Send data to server
      axios
        .get(BASEURL, {
          params: {
            method: 'set_entry',
            input_type: INPUT_TYPE,
            response_type: RESPONSE_TYPE,
            rest_data: {
              //Get the session id from postman login api request and replace the below id
              session: session_value,
              module_name: this.state.MODULE_NAME,
              name_value_list: {
                first_name: {name: 'first_name', value: this.state.firstName},
                last_name: {name: 'last_name', value: this.state.lastName},
                website: {name: 'website', value: this.state.website},
                status: {name: 'status', value: this.state.status},
              },
            },
          },
        })
        .then((resp) => {
          this.setState({
            visible: true,
            firstName: '',
            lastName: '',
            website: '',
            status: '',
            MODULE_NAME: '',
            snackMsg: 'Data Added Successfully!',
          });
        })
        .catch((err) => console.log('ERROR: ', err));
    } else {
      this.setState({
        visible: true,
        snackMsg: `Please Login To Create New ${this.state.MODULE_NAME}`,
      });
    }
  };

  render() {
    return (
      <View>
        <View style={styles.fullNameStyle}>
          <TextInput
            style={styles.firstName}
            mode="outlined"
            label="First Name"
            value={this.state.firstName}
            theme={{colors: {primary: 'orange', underlineColor: 'transparent'}}}
            onChangeText={(text) => this.setState({firstName: text})}
          />
          <TextInput
            style={styles.lastName}
            mode="outlined"
            label="Last Name"
            value={this.state.lastName}
            theme={{colors: {primary: 'orange', underlineColor: 'transparent'}}}
            onChangeText={(text) => this.setState({lastName: text})}
          />
        </View>
        <TextInput
          label="Website"
          style={styles.website}
          mode="outlined"
          value={this.state.website}
          theme={{colors: {primary: 'orange', underlineColor: 'transparent'}}}
          onChangeText={(text) => this.setState({website: text})}
        />
        <Picker
          selectedValue={this.state.status}
          onValueChange={(item, index) => this.setState({status: item})}>
          <Picker.Item label="Choose Status" value="" />
          <Picker.Item label="New" value="New" />
          <Picker.Item label="Assigned" value="Assigned" />
          <Picker.Item label="In Process" value="In Process" />
          <Picker.Item label="Converted" value="Converted" />
          <Picker.Item label="Dead" value="Dead" />
        </Picker>
        <Button
          disabled={
            this.state.firstName
              ? this.state.lastName
                ? this.state.website
                  ? this.state.status
                    ? false
                    : true
                  : true
                : true
              : true
          }
          color="orange"
          style={styles.createButton}
          mode="contained"
          onPress={this.createNewRecord}>
          Create
        </Button>
        <Snackbar
          style={{marginTop: 900}}
          duration={1000}
          visible={this.state.visible}
          onDismiss={this._onDismissSnackBar}>
          {this.state.snackMsg}
        </Snackbar>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fullNameStyle: {
    flexDirection: 'row',
  },
  firstName: {
    flex: 1,
    margin: 7,
    borderColor: 'red',
  },
  lastName: {
    flex: 1,
    margin: 7,
  },
  website: {
    margin: 7,
  },
  status: {
    margin: 7,
    borderWidth: 2,
    borderColor: 'black',
    borderBottomColor: 'red',
  },
  createButton: {
    marginTop: 10,
    marginBottom: 100,
    width: 150,
    alignSelf: 'center',
  },
});
